import os

from xml.etree import ElementTree as ET
from xml_utils.XmlUnpacker import XmlUnpacker
from xml_utils.XmlPacker import XmlPacker
from _base_section import *



__all__ = ('Base_XML_Section',)



def prettify(elem):
	from xml.dom import minidom
	reparsed = minidom.parseString(ET.tostring(elem))
	return reparsed.toprettyxml()



class Base_XML_Section(Base_Section):
	def from_bin_stream(self, stream, row):
		assert row.header == self.header, row.header
		self._data = XmlUnpacker().read(stream, row.position)
		assert stream.tell() == row.position + row.length, (stream.tell(), row.position + row.length)
		self._exist = True

	def unp_to_dir(self, unp_dir):
		with open(os.path.join(unp_dir, '%s.xml' % self.header), 'w') as fw:
			fw.write(prettify(self._data))

	def from_dir(self, unp_dir):
		try:
			self._data = ET.parse(os.path.join(unp_dir, '%s.xml' % self.header)).getroot()
			self._exist = True
		except:
			self._exist = False

	def to_bin(self):
		return XmlPacker().pack(self._data)

	def setDataForPyQt(self, treeWidget, strings, QTreeWidgetItem):
		def rec(obj, is_root, parent):
			for a in obj:
				if is_root:
					root_item = QTreeWidgetItem(parent)
					root_item.setText(0, a.tag)
					rec(a, False, root_item)
					treeWidget.addTopLevelItem(root_item)
				else:
					it = QTreeWidgetItem(parent)
					it.setText(0, a.tag)
					it.setText(1, a.text)
					rec(a, False, it)
		rec(self._data, True, treeWidget)
