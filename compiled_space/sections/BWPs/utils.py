from _base_json_section import *



class ParticleInfo(CStructure):
	_size_ = 76

	_fields_ = [
		('transform',          c_float * 16 ),
		('resource_fnv',       c_uint32     ),
		('reflection_visible', c_uint32, 1  ),
		('pad',                c_uint32, 31 ),
		('seed_time',          c_float      ), # 0.1 by default
		]

	_tests_ = {
		# ...
		#'pad': { '==': 0 },
		'seed_time': { '>=': 0.0 },
		}
