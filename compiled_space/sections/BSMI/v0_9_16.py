﻿""" BSMI (Model Instances) """

from _base_json_section import *
from .v0_9_12 import ChunkModel, ModelAnimation



class BSMI_Section_0_9_16(Base_JSON_Section):
	header = 'BSMI'
	int1 = 1

	_fields_ = [
		(list, 'transforms',       '<16f'         ),
		(list, 'chunk_models',     ChunkModel     ),
		(list, 'visibility_masks', '<I'           ), # 0.9.12: BWSV (visibilityMask)
		(list, 'bsmo_models_id',   '<I'           ),
		(list, 'model_animation',  ModelAnimation ),
		(list, 'animations_id',    '<i'           ),
		(list, '7_12',             '<3I'          ), # 0.9.12: WSMI['1_12']
		(list, '8_4',              '<I'           ),
		(list, '9_20',             '<5f'          ),
		]
