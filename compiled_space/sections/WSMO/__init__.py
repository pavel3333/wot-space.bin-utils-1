""" WSMO (WoT Static Models) """

from WSMO.utils import *



__all__ = ('WSMO_Section_0_9_12',)



class WSMO_Section_0_9_12(Base_JSON_Section):
	header = 'WSMO'
	int1 = 1

	_fields_ = [
		(list, '1', '<Ii'          ),
		(list, '2', Section_WSMO_2 ), # destructibles.xml/fallingAtoms
		(list, '3', Section_WSMO_3 ), # destructibles.xml/fragiles, destructibles.xml/structures
		(list, '4', '<16f'         ),
		(list, '5', '<I'           ),
		]
