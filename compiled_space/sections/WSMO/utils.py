from _base_json_section import *



class Section_WSMO_2(CStructure):
	'''
	destructibles.xml/fallingAtoms
	'''
	_size_ = 44

	_fields_ = [
		('lifetime_effect_fnv',  c_uint32    ), # lifetimeEffect
		('fracture_effect_fnv',  c_uint32    ), # fractureEffect
		('touchdown_effect_fnv', c_uint32    ), # touchdownEffect
		('effect_scale',         c_float     ), # effectScale
		('physic_params',        c_float * 7 ), # physicParams
		]



class Section_WSMO_3(CStructure):
	'''
	destructibles.xml/fragiles
	destructibles.xml/structures
	'''
	_size_ = 36

	_fields_ = [
		('lifetime_effect_fnv',   c_uint32 ), # lifetimeEffect
		('effect_fnv',            c_uint32 ), # effect / ramEffect
		('decay_effect_fnv',      c_uint32 ), # decayEffect
		('hit_effect_fnv',        c_uint32 ), # hitEffect
		('_4',                    c_float  ),
		('effect_scale',          c_float  ), # effectScale
		('id_1',                  c_int32  ),
		('id_2',                  c_int32  ),
		('entry_type',            c_uint32 ), # fragiles:0, structures:1
		]

	_tests_ = {
		'_4': { '==': 0.0 },
		'id_1': { '>=': -1 },
		'id_2': { '>=': -1 },
		'entry_type': { 'in': (0, 1) },
		}
