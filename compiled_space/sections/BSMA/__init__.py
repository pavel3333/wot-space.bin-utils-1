""" BSMA (Static Materials) """

from BSMA.utils import *



__all__ = ('BSMA_Section_0_9_12', 'BSMA_Section_1_0_0')



class BSMA_Section_0_9_12(Base_JSON_Section):
	header = 'BSMA'
	int1 = 1

	_fields_ = [
		(list, 'materials', MaterialInfo ),
		(list, 'fx',        '<I'         ),
		(list, 'props',     PropertyInfo ),
		(list, 'matrices',  '<16f'       ),
		(list, 'vectors',   '<4f'        ),
		]



class BSMA_Section_1_0_0(Base_JSON_Section):
	header = 'BSMA'
	int1 = 1

	@row_seek(True)
	def from_bin_stream(self, stream, row):
		self._data = Dict()
		self._data['materials'] = self.read_entries(stream, MaterialInfo)
		self._data['fx'] = self.read_entries(stream, 4, '<I')
		self._data['props'] = self.read_entries(stream, PropertyInfo)
		self._data['matrices'] = self.read_entries(stream, 64, '<16f')
		self._data['vectors'] = self.read_entries(stream, 16, '<4f')
		textures = []
		textures_cnt = unpack('<I', stream.read(4))[0]
		for _ in range(textures_cnt):
			tex_fnv, _2, mip_map_count, length = unpack('<4I', stream.read(16))
			assert _2 in (0, 1, 2, 3, 4, 5, 6, 7), _2
			data = stream.read(length)
			assert data[:4] == b'DDS '
			header = DDS_HEADER(data[4:128]).to_dict()
			assert mip_map_count == header['dwMipMapCount'], mip_map_count
			texformat_len = unpack('<I', stream.read(4))[0]
			texformat = stream.read(texformat_len)
			str_length = unpack('<I', stream.read(4))[0]
			str_data = stream.read(str_length).decode('ascii')
			textures.append({
				'tex_fnv': tex_fnv,
				'_2': _2,
				'mip_map_count': mip_map_count,
				'length': length,
				'dds_header': header,
				'dds_data': data[128:].decode('latin1'),
				'texformat_len': texformat_len,
				'texformat': texformat.decode('latin1'),
				'str_length': str_length,
				'str_data': str_data,
			})
		self._data['textures'] = textures

	def to_bin(self):
		res = self.write_entries(self._data['materials'], MaterialInfo)
		res += self.write_entries(self._data['fx'], 4, '<I')
		res += self.write_entries(self._data['props'], PropertyInfo)
		res += self.write_entries(self._data['matrices'], 64, '<16f')
		res += self.write_entries(self._data['vectors'], 16, '<4f')
		res += pack('<I', len(self._data['textures']))
		for tex in self._data['textures']:
			res += pack(
				'<4I',
				tex['tex_fnv'],
				tex['_2'],
				tex['mip_map_count'],
				tex['length']
			)
			res += b'DDS '
			res += DDS_HEADER(tex['dds_header']).to_bin()
			res += tex['dds_data'].encode('latin1')
			res += pack('<I', tex['texformat_len'])
			res += tex['texformat'].encode('latin1')
			res += pack('<I', tex['str_length'])
			res += tex['str_data'].encode('ascii')
		return res

	def init(self):
		pass

	def from_visual_material(self, bwst, material):
		'''
		.visual/renderSet/geometry/primitiveGroup/material
		'''
		key_from = len(self._data['props'])
		key_fx = 0
		def get_type_by_prop(sec):
			if sec.has_key('Bool'):
				value = int(sec['Bool'].asBool)
				vt = 1
			elif sec.has_key('Float'):
				value = sec['Float'].asFloat
				vt = 2
			elif sec.has_key('Int'):
				value = sec['Int'].asInt
				vt = 3
			elif sec.has_key('Vector4'):
				value = len(self._data['vectors'])
				self._data['vectors'].append(sec['Vector4'].asVector4.tuple())
				vt = 5
			elif sec.has_key('Texture'):
				value = bwst.add_str(sec['Texture'].asString)
				vt = 6
			else:
				assert False
			prop_fnv = bwst.add_str(sec.asString)
			return {'prop_fnv': prop_fnv, 'value': value, 'value_type': vt}

		for name, sect in material.items():
			if name == 'fx':
				_fx_hash = bwst.add_str(sect.asString)
				if _fx_hash not in self._data['fx']:
					key_fx = len(self._data['fx'])
					self._data['fx'].append(_fx_hash)
				else:
					key_fx = self._data['fx'].index(_fx_hash)
			elif name == 'property':
				self._data['props'].append(get_type_by_prop(sect))
		key_to = len(self._data['props']) - 1
		self._data['materials'].append({
			'key_fx': key_fx,
			'key_from': key_from,
			'key_to': key_to,
		})
