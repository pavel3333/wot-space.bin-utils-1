""" BWT2 (Terrain 2) """

from _base_json_section import *
from .v0_9_12 import ChunkTerrain_v0_9_12
from .v0_9_20 import TerrainSettings1_v0_9_20



class OutlandCascade_v1_0_0(CStructure):
	_size_ = 40

	_fields_ = [
		('extent_min',     c_float * 3 ),
		('extent_max',     c_float * 3 ),
		('height_map_fnv', c_uint32    ),
		('normal_map_fnv', c_uint32    ),
		('tile_map_fnv',   c_uint32    ),
		('tile_scale',     c_float     ),
		]



class BWT2_Section_1_0_0(Base_JSON_Section):
	header = 'BWT2'
	int1 = 3

	_fields_ = [
		(dict, 'settings',      TerrainSettings1_v0_9_20 ),
		(list, 'cdatas',        ChunkTerrain_v0_9_12     ),
		(list, '3',             '<i'                     ),
		(dict, 'settings2',     '<35I'                   ),
		(list, 'lod_distances', '<f'                     ),
		(list, '6',             '<2i'                    ),
		(list, 'cascades',      OutlandCascade_v1_0_0    ), # outland/cascade
		(list, 'tiles_fnv',     '<I'                     ), # outland/tiles
		(dict, '9',             '<I4fI'                  ),
		(list, '10',            '<6f'                    ),
		(list, '11',            '<3f'                    ),
		(list, '12',            '<4I'                    ),
		(list, '13',            '<i'                     ),
		(list, '14',            '<h'                     ),
		(list, '15',            '<I'                     ),
		(list, '16',            '<I'                     ),
		]
