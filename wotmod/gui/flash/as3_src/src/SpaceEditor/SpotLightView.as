package SpaceEditor
{
	import SpaceEditor.components.*;
	import net.wg.gui.components.controls.*;

	public class SpotLightView extends BaseView
	{
		private var cast_shadows: CheckBox;
		private var colour: ColorChooser;
		private var position: Vector3Input;
		private var direction: Vector3Input;
		private var inner_radius: NumericStepper;
		private var outer_radius: NumericStepper;
		private var multiplier: NumericStepper;
		private var cone_angle: NumericStepper;

		public function SpotLightView()
		{
			super();

			cast_shadows = getComponent("CheckBox", CheckBox , {
				label: "castShadows",
				y: 10
			});

			colour = getColorChooser("colour:", 40);

			position = getVector3Input("position:", 100);

			direction = getVector3Input("direction:", 160);

			inner_radius = getNumericStepper("innerRadius:", 220);

			outer_radius = getNumericStepper("outerRadius:", 250);

			multiplier = getNumericStepper("multiplier:", 280);

			cone_angle = getNumericStepper("coneAngle:", 310);
		}

		override public function setData(data: Object) : void
		{
			cast_shadows.selected = data.cast_shadows;
			colour.value = data.colour;
			position.value = data.position;
			direction.value = data.direction;
			inner_radius.value = data.inner_radius;
			outer_radius.value = data.outer_radius;
			multiplier.value = data.multiplier;
			cone_angle.value = data.cone_angle;
		}

		override public function getData() : Object
		{
			var data: Object = new Object();
			data.cast_shadows = cast_shadows.selected;
			data.colour = colour.value;
			data.position = position.value;
			data.direction = direction.value;
			data.inner_radius = inner_radius.value;
			data.outer_radius = outer_radius.value;
			data.multiplier = multiplier.value;
			data.cone_angle = cone_angle.value;
			return data;
		}
	}
}
