package SpaceEditor.components 
{
	import flash.display.Sprite;
	import net.wg.gui.components.controls.TextInput;

	public class Vector3Input extends BaseUtils
	{
		private var _input_x: TextInput;
		private var _input_y: TextInput;
		private var _input_z: TextInput;

		public function Vector3Input() 
		{
			super();

			_input_x = getNumericInput({
				x: 0,
				y: y,
				width: 90
			});

			_input_y = getNumericInput({
				x: _input_x.width + 5,
				y: y,
				width: 90
			});

			_input_z = getNumericInput({
				x: _input_y.x + _input_y.width + 5,
				y: y,
				width: 90
			});
		}

		public function set value(n:Array):void
		{
			_input_x.text = Number(n[0]).toPrecision(6);
			_input_y.text = Number(n[1]).toPrecision(6);
			_input_z.text = Number(n[2]).toPrecision(6);
		}

		public function get value():Array
		{
			return [
				Number(_input_x.text),
				Number(_input_y.text),
				Number(_input_z.text)
			];
		}
	}
}
