package SpaceEditor.components 
{
	import flash.display.Sprite;
	import flash.events.Event;
	import net.wg.gui.components.controls.*;
	import net.wg.gui.components.advanced.TextAreaSimple;

	public class BaseUtils extends Sprite
	{
		
		public function BaseUtils() 
		{
			super();
		}

		protected function getNumericStepper(label: String, y: uint, max: Number=1000.0, min: Number=0.0) : NumericStepper
		{
			getLabelControl({
				text: label,
				height: 35,
				y: y
			});
			var comp: NumericStepper = getCompNumericStepper({
				maximum: max,
				minimum: min,
				stepSize: 0.00001,
				x: 85,
				y: y
			});
			comp.setActualSize(120, comp.actualHeight);
			return comp;
		}
		
		protected function getNumericInput(p1: Object) : TextInput
		{
			var txtInput: TextInput = getComponent("TextInput", TextInput, p1);
			txtInput.addEventListener(Event.CHANGE, validate, false, 0, true);
			return txtInput;
			function validate(e: Event):void
			{
				var target: TextInput = e.currentTarget as TextInput;
				var currentString:String = target.text;
				var numbersRegex:RegExp = /^[+-]?([0-9]*[.])?[0-9]+$/;
				var invalidRegex:RegExp = /[^0-9.]/;

				if(numbersRegex.test(currentString) == false)
				{
					currentString = currentString.replace(invalidRegex, "");
				}
				target.text = currentString;
			}
		}

		protected function getCompNumericStepper(p1: Object) : NumericStepper
		{
			return getComponent("NumericStepper", NumericStepper , p1);
		}

		protected function getComponent(p1: String, p2: Class, p3: Object=null) : *
		{
			return addChild(App.utils.classFactory.getComponent(p1, p2 , p3)) as p2;
		}

		protected function getLabelControl(p1: Object) : void
		{
			getComponent("LabelControl", LabelControl, p1);
		}

		protected function getColorChooser(label: String, y: uint) : ColorChooser
		{
			getLabelControl({
				text: label,
				y: y
			});
			var col: ColorChooser = new ColorChooser();
			col.usePopup = true;
			col.y = y + 20;
			return addChild(col) as ColorChooser;
		}

		protected function getVector3Input(label: String, y: uint, on_new_line: Boolean = true) : Vector3Input
		{
			getLabelControl({
				text: label,
				y: y
			});
			var vec3input: Vector3Input = new Vector3Input();
			if (on_new_line) {
				vec3input.y = y + 20;
			} else {
				vec3input.x = 36;
				vec3input.y = y;
			}
			return addChild(vec3input) as Vector3Input;
		}

		protected function getTransformInput(label: String, y: uint) : TransformInput
		{
			getLabelControl({
				text: label,
				y: y
			});
			var transformInput: TransformInput = new TransformInput();
			transformInput.y = y + 20;
			return addChild(transformInput) as TransformInput;
		}

		protected function getTextInput(label: String, y: uint) : TextInput
		{
			getLabelControl({
				text: label,
				y: y
			});
			return getComponent("TextInput", TextInput , {
				width: 300,
				y: y + 20
			});
		}
	}
}