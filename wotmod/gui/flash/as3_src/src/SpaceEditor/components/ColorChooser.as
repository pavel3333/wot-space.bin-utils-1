/**
 * ColorChooser.as
 * Keith Peters
 * version 0.9.10
 * 
 * A Color Chooser component, allowing textual input, a default gradient, or custom image.
 * 
 * Copyright (c) 2011 Keith Peters
 * 
 * popup color choosing code by Rashid Ghassempouri
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
 
package SpaceEditor.components
{
	import flash.display.*;
	import flash.events.*;
	import flash.geom.*;
	import flash.text.*;
	import net.wg.gui.components.controls.NumericStepper;
	import scaleform.clik.events.InputEvent;
	
	

	[Event(name="change", type="flash.events.Event")]
	public class ColorChooser extends Component
	{
		public static const TOP:String = "top";
		public static const BOTTOM:String = "bottom";
		
		protected var _colors:BitmapData;
		protected var _colorsContainer:Sprite;
		protected var _defaultModelColors:Array=[0xFF0000, 0xFFFF00, 0x00FF00, 0x00FFFF, 0x0000FF, 0xFF00FF, 0xFF0000,0xFFFFFF,0x000000];
		
		protected var _input_r:NumericStepper;
		protected var _input_g:NumericStepper;
		protected var _input_b:NumericStepper;
		
		protected var _model:DisplayObject;
		protected var _oldColorChoice:uint = 0;
		protected var _popupAlign:String = BOTTOM;
		protected var _stage:Stage;
		protected var _swatch:Sprite;
		protected var _tmpColorChoice:uint = 0;
		protected var _usePopup:Boolean = false;
		protected var _value: Object = {
			r: 0.0,
			g: 0.0,
			b: 0.0
		};
		
		/**
		 * Constructor
		 * @param parent The parent DisplayObjectContainer on which to add this ColorChooser.
		 * @param xpos The x position to place this component.
		 * @param ypos The y position to place this component.
		 * @param value The initial color value of this component.
		 * @param defaultHandler The event handling function to handle the default event for this component (change in this case).
		 */

		public function floats_to_uint(floats: Object) : uint
		{
			return uint(
				uint(floats.r) << 16 |
				uint(floats.g) << 8  |
				uint(floats.b)
			);
		}
 
		public function uint_to_floats(col: uint) : Object
		{
			return {
				r: ((col >> 16) & 0xff),
				g: ((col >> 8) & 0xff),
				b: (col & 0xff)
			};
		}

		public function ColorChooser(parent:DisplayObjectContainer = null, xpos:Number = 0, ypos:Number =  0, value:uint = 0xff0000, defaultHandler:Function = null)
		{
			_oldColorChoice = _tmpColorChoice = value;
			_value = uint_to_floats(value);

			super(parent, xpos, ypos);
			
			if(defaultHandler != null)
			{
				addEventListener(Event.CHANGE, defaultHandler);
			}
				
		}		
		
		/**
		 * Initializes the component.
		 */
		override protected function init():void
		{
			
			super.init();

			_width = 65;
			_height = 15;
			value = _value;
		}
		
		override protected function addChildren():void
		{
			_swatch = new Sprite();
			_swatch.filters = [getShadow(2, true)];
			addChild(_swatch);

			_input_r = getCompNumericStepper({
				maximum: 255.0,
				minimum: 0.0,
				stepSize: 0.5,
				x: 24 + 5,
				y: y
			});
			_input_r.setActualSize(80, _input_r.actualHeight);

			_input_g = getCompNumericStepper({
				maximum: 255.0,
				minimum: 0.0,
				stepSize: 0.5,
				x: _input_r.x + _input_r.width + 5,
				y: y
			});
			_input_g.setActualSize(80, _input_g.actualHeight);

			_input_b = getCompNumericStepper({
				maximum: 255.0,
				minimum: 0.0,
				stepSize: 0.5,
				x: _input_g.x + _input_g.width + 5,
				y: y
			});
			_input_b.setActualSize(80, _input_b.actualHeight);

			_input_r.addEventListener(InputEvent.INPUT, onChange, false, 0, true);
			_input_g.addEventListener(InputEvent.INPUT, onChange, false, 0, true);
			_input_b.addEventListener(InputEvent.INPUT, onChange, false, 0, true);
			
			_colorsContainer = new Sprite();
			_model = getDefaultModel();
			drawColors(_model);
		}
		
		///////////////////////////////////
		// public methods
		///////////////////////////////////
		
		/**
		 * Draws the visual ui of the component.
		 */
		override public function draw():void
		{
			super.draw();
			_swatch.graphics.clear();
			_swatch.graphics.beginFill(floats_to_uint(_value));
			_swatch.graphics.drawRect(2, 2, 20, 20);
			_swatch.graphics.endFill();
		}
		
		///////////////////////////////////
		// event handlers
		///////////////////////////////////
		
		/**
		 * Internal change handler.
		 * @param event The Event passed by the system.
		 */
		protected function onChange(event:InputEvent):void
		{
			event.stopImmediatePropagation();
			_value = {
				r: _input_r.value,
				g: _input_g.value,
				b: _input_b.value
			};
			_oldColorChoice = floats_to_uint(value);
			invalidate();
			dispatchEvent(new Event(Event.CHANGE));
			
		}	
		
		///////////////////////////////////
		// getter/setters
		///////////////////////////////////
		
		/**
		 * Gets / sets the color value of this ColorChooser.
		 */
		public function set value(n:Object):void
		{
			_input_r.value = n.r;
			_input_g.value = n.g;
			_input_b.value = n.b;
			_value = n;
			invalidate();
		}
		public function get value():Object
		{
			return _value;
		}
		
		///////////////////////////////////
		// COLOR PICKER MODE SUPPORT
		///////////////////////////////////}
		
		
		public function get model():DisplayObject { return _model; }
		public function set model(value:DisplayObject):void 
		{
			_model = value;
			if (_model!=null) {
				drawColors(_model);
				if (!usePopup) usePopup = true;
			} else {
				_model = getDefaultModel();
				drawColors(_model);
				usePopup = false;
			}
		}
		
		protected function drawColors(d:DisplayObject):void{
			_colors = new BitmapData(d.width, d.height);
			_colors.draw(d);
			while (_colorsContainer.numChildren) _colorsContainer.removeChildAt(0);
			_colorsContainer.addChild(new Bitmap(_colors));
			placeColors();
		}
		
		public function get popupAlign():String { return _popupAlign; }
		public function set popupAlign(value:String):void {
			_popupAlign = value;
			placeColors();
		}
		
		public function get usePopup():Boolean { return _usePopup; }
		public function set usePopup(value:Boolean):void {
			_usePopup = value;
			
			_swatch.buttonMode = true;
			_colorsContainer.buttonMode = true;
			_colorsContainer.addEventListener(MouseEvent.MOUSE_MOVE, browseColorChoice);
			_colorsContainer.addEventListener(MouseEvent.MOUSE_OUT, backToColorChoice);
			_colorsContainer.addEventListener(MouseEvent.CLICK, setColorChoice);
			_swatch.addEventListener(MouseEvent.CLICK, onSwatchClick);
			
			if (!_usePopup) {
				_swatch.buttonMode = false;
				_colorsContainer.buttonMode = false;
				_colorsContainer.removeEventListener(MouseEvent.MOUSE_MOVE, browseColorChoice);
				_colorsContainer.removeEventListener(MouseEvent.MOUSE_OUT, backToColorChoice);
				_colorsContainer.removeEventListener(MouseEvent.CLICK, setColorChoice);
				_swatch.removeEventListener(MouseEvent.CLICK, onSwatchClick);
			}
		}
		
		/**
		 * The color picker mode Handlers 
		 */
		
		
		protected function onSwatchClick(event:MouseEvent):void 
		{
			event.stopImmediatePropagation();
			displayColors();
		}
		
		protected function backToColorChoice(e:MouseEvent):void 
		{
			value = uint_to_floats(_oldColorChoice);
		}
		
		protected function setColorChoice(e:MouseEvent):void {
			value = uint_to_floats(_colors.getPixel(_colorsContainer.mouseX, _colorsContainer.mouseY));
			_oldColorChoice = floats_to_uint(value);
			dispatchEvent(new Event(Event.CHANGE));
			displayColors();
		}
		
		protected function browseColorChoice(e:MouseEvent):void 
		{
			_tmpColorChoice = _colors.getPixel(_colorsContainer.mouseX, _colorsContainer.mouseY);
			value = uint_to_floats(_tmpColorChoice);
		}

		/**
		 * The color picker mode Display functions
		 */
		
		protected function displayColors():void 
		{
			placeColors();
			if (_colorsContainer.parent) _colorsContainer.parent.removeChild(_colorsContainer);
			else parent.addChild(_colorsContainer);
		}		
		
		protected function placeColors():void{
			//var point:Point = new Point(x, y);
			//if(parent) point = parent.localToGlobal(point);
			switch (_popupAlign)
			{
				case TOP : 
					_colorsContainer.x = x;
					_colorsContainer.y = y - _colorsContainer.height - 4;
				break;
				case BOTTOM : 
					_colorsContainer.x = x;
					_colorsContainer.y = y + 22;
				break;
				default: 
					_colorsContainer.x = x;
					_colorsContainer.y = y + 22;
				break;
			}
		}
		
		/**
		 * Create the default gradient Model
		 */

		protected function getDefaultModel():Sprite {	
			var w:Number = 100;
			var h:Number = 100;
			var bmd:BitmapData = new BitmapData(w, h);
			
			var g1:Sprite = getGradientSprite(w, h, _defaultModelColors);
			bmd.draw(g1);
					
			var blendmodes:Array = [BlendMode.MULTIPLY,BlendMode.ADD];
			var nb:int = blendmodes.length;
			var g2:Sprite = getGradientSprite(h/nb, w, [0xFFFFFF, 0x000000]);		
			
			for (var i:int = 0; i < nb; i++) {
				var blendmode:String = blendmodes[i];
				var m:Matrix = new Matrix();
				m.rotate(-Math.PI / 2);
				m.translate(0, h / nb * i + h/nb);
				bmd.draw(g2, m, null,blendmode);
			}
			
			var s:Sprite = new Sprite();
			var bm:Bitmap = new Bitmap(bmd);
			s.addChild(bm);
			return(s);
		}
		
		protected function getGradientSprite(w:Number, h:Number, gc:Array):Sprite 
		{
			var gs:Sprite = new Sprite();
			var g:Graphics = gs.graphics;
			var gn:int = gc.length;
			var ga:Array = [];
			var gr:Array = [];
			var gm:Matrix = new Matrix(); gm.createGradientBox(w, h, 0, 0, 0);
			for (var i:int = 0; i < gn; i++) { ga.push(1); gr.push(0x00 + 0xFF / (gn - 1) * i); }
			g.beginGradientFill(GradientType.LINEAR, gc, ga, gr, gm, SpreadMethod.PAD,InterpolationMethod.RGB);
			g.drawRect(0, 0, w, h);
			g.endFill();	
			return(gs);
		}
	}
}