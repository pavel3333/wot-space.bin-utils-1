package SpaceEditor
{
	import flash.display.*;
	import net.wg.gui.components.advanced.*;
	import net.wg.gui.components.controls.*;
	import net.wg.infrastructure.base.*;
	import net.wg.infrastructure.events.*;
	import scaleform.clik.controls.*;
	import scaleform.clik.data.*;
	import scaleform.clik.events.*;

	public class SpaceEditorWindow extends AbstractWindowView
    {
		private var scrollingList: ScrollingList;
		private var fieldSet: FieldSet;
		private var updateBtn: SoundButton;
		private var currentView: BaseView;
		private var selectedItem: Object;

		private const type_to_class: Array = [
			AudioView,
			ModelView,
			OmniLightView,
			ParticlesView,
			PulseLightView,
			PulseSpotLightView,
			SpeedtreeView,
			SpotLightView
		];

		private var dp: DataProvider;

		public var py_get_data: Function = null;
		public var py_update: Function = null;
		public var py_log: Function = null;

        public function SpaceEditorWindow()
        {
            super();

			canMinimize = true;
			enabledCloseBtn = false;
			
			//showWindowBgForm = false;
			//showWindowBg = false;

			setSize(350, App.appHeight - 50);
            App.instance.loaderMgr.loadLibraries(Vector.<String>([
                "guiControlsLobby.swf"
            ]));
			App.instance.loaderMgr.addEventListener(LibraryLoaderEvent.LOADED_COMPLETED, onLoadedCompleted, false, 0, true);
        }

		public function onWindowMinimize2() : void
		{
			window.alpha = scrollingList.visible ? 0.05 : 1.0;
			scrollingList.visible = !scrollingList.visible;
			fieldSet.visible = !fieldSet.visible;
			updateBtn.visible = !updateBtn.visible;
		}

		private function onLoadedCompleted() : void
		{
			try
			{
				scrollingList = getComponent("ScrollingList", ScrollingList, {
					x: 2,
					y: 2,
					width: this.width - 4,
					height: this.height - 400,
					enabled: true,
					enableInitCallback: false,
					focusable: true,
					scrollBar: "ScrollBar",
					itemRendererName: "DropDownListItemRendererSound",
					labelField: "label",
					rowHeight: 26,
					dataProvider: dp
				});

				fieldSet = getComponent("FieldSet", FieldSet , {
					label: "Properties",
					x: 0,
					y: scrollingList.height,
					width: this.width,
					height: 380
				});

				updateBtn = getComponent("ButtonNormal", SoundButton , {
					label: "Update",
					enabled: false,
					y: this.height - 22
				});
				updateBtn.x = (width - updateBtn.width) / 2;

				scrollingList.addEventListener(ListEvent.INDEX_CHANGE, onIndexChange, false, 0, true);
				updateBtn.addEventListener(ButtonEvent.CLICK, onUpdate, false, 0, true);
			}
			catch (err: Error)
			{
				this.py_log(err.getStackTrace());
			}
		}

        public function setDataFromPy(arr: Array) : void
        {
			dp = new DataProvider(arr);
        }

        private function onUpdate(event: ButtonEvent) : void
        {
			if (currentView) {
				var viewData: Object = currentView.getData();
				this.py_update(selectedItem, viewData);
			}
		}

        private function onIndexChange(event: ListEvent) : void
        {
			try
			{
				selectedItem = scrollingList.dataProvider.requestItemAt(event.index);

				if (currentView) {
					currentView.clear();
					currentView = null;
					updateBtn.enabled = false;
				}

				var viewData: Object = this.py_get_data(selectedItem);
				if (viewData != null) {
					var cls: Class =  type_to_class[selectedItem.type]; 
					currentView = new cls();
					currentView.setData(viewData);
					currentView.scrollPane.x = 15;
					currentView.scrollPane.y = 20;
					currentView.scrollPane.setSize(fieldSet.width-30, fieldSet.height);
					fieldSet.addChild(currentView.scrollPane);
					updateBtn.enabled = true;
				}
			}
			catch (err: Error)
			{
				this.py_log(err.getStackTrace());
			}
        }

        override protected function onPopulate():void
        {
            super.onPopulate();
			window.x = App.appWidth - width - 10;
            window.title = "Hangar Editor - SkepticalFox";
			window.useBottomBtns = true;
			window.alpha = 1.0;
        }

		private function getComponent(p1: String, p2: Class, p3: Object=null) : *
		{
			return addChild(App.utils.classFactory.getComponent(p1, p2 , p3)) as p2;
		}
    }
}
