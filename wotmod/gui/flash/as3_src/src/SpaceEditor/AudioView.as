package SpaceEditor
{
	import net.wg.gui.components.controls.*;

	public class AudioView extends BaseView
	{
		private var _event_name: TextInput;
		private var _max_distance: NumericStepper;

		public function AudioView()
		{
			super();

			_event_name = getTextInput("eventName:", 10);

			_max_distance = getNumericStepper("maxDistance:", 70);
		}

		override public function setData(data: Object) : void
		{
			_event_name.text = data.event_name;
			_max_distance.value = data.max_distance;
		}

		override public function getData() : Object
		{
			var data: Object = new Object();
			data.event_name = _event_name.text;
			data.max_distance = _max_distance.value;
			return data;
		}
	}
}
