package SpaceEditor
{
	import SpaceEditor.components.*;
	import net.wg.gui.components.controls.*;

	public class OmniLightView extends BaseView
	{
		private var cast_shadows: CheckBox;
		private var colour: ColorChooser;
		private var position: Vector3Input;
		private var inner_radius: NumericStepper;
		private var outer_radius: NumericStepper;
		private var multiplier: NumericStepper;

		public function OmniLightView()
		{
			super();

			cast_shadows = getComponent("CheckBox", CheckBox , {
				label: "castShadows",
				y: 10
			});

			colour = getColorChooser("colour:", 40);

			position = getVector3Input("position:", 100);

			inner_radius = getNumericStepper("innerRadius:", 160);

			outer_radius = getNumericStepper("outerRadius:", 190);

			multiplier = getNumericStepper("multiplier:", 220);
		}

		override public function setData(data: Object) : void
		{
			cast_shadows.selected = data.cast_shadows;
			colour.value = data.colour;
			position.value = data.position;
			inner_radius.value = data.inner_radius;
			outer_radius.value = data.outer_radius;
			multiplier.value = data.multiplier;
		}

		override public function getData() : Object
		{
			var data: Object = new Object();
			data.cast_shadows = cast_shadows.selected;
			data.colour = colour.value;
			data.position = position.value;
			data.inner_radius = inner_radius.value;
			data.outer_radius = outer_radius.value;
			data.multiplier = multiplier.value;
			return data;
		}
	}
}
