package SpaceEditor
{
	import net.wg.gui.components.controls.*;

	public class SpeedtreeView extends BaseView
	{
		private var casts_shadow: CheckBox;
		private var casts_local_shadow: CheckBox;
		private var reflection_visible: CheckBox;

		public function SpeedtreeView()
		{
			super();

			casts_shadow = getComponent("CheckBox", CheckBox , {
				label: "castsShadow",
				y: 10
			});

			casts_local_shadow = getComponent("CheckBox", CheckBox , {
				label: "castsLocalShadow",
				y: 40
			});

			reflection_visible = getComponent("CheckBox", CheckBox , {
				label: "reflectionVisible",
				y: 70
			});
		}

		override public function setData(data: Object) : void
		{
			casts_shadow.selected = data.casts_shadow;
			casts_local_shadow.selected = data.casts_local_shadow;
			reflection_visible.selected = data.reflection_visible;
		}

		override public function getData() : Object
		{
			var data: Object = new Object();
			data.casts_shadow = casts_shadow.selected;
			data.casts_local_shadow = casts_local_shadow.selected;
			data.reflection_visible = reflection_visible.selected;
			return data;
		}
	}
}
