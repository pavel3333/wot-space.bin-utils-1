﻿''' ShadowHunterRUS - 2018 '''

print('mod_offline_hangar: https://koreanrandom.com/forum/topic/29351-/')



import ResMgr
import os

from Math import Matrix

from gui.ClientHangarSpace import ClientHangarSpace
from gui.app_loader import g_appLoader
from gui.Scaleform.framework.entities.abstract.AbstractWindowView import AbstractWindowView
from gui.Scaleform.framework import ViewTypes, ScopeTemplates, g_entitiesFactories, ViewSettings
from gui.Scaleform.framework.managers.loaders import SFViewLoadParams
from gui.Scaleform.daapi.view.login.LoginView import LoginView
from gui.Scaleform.framework.entities.View import View



def getArrFromTransform(value):
	mat = value.readMatrix('transform')
	arr_of_arr = [[], [], [], []]
	for i in range(0, 4):
		for j in range(0, 3):
			arr_of_arr[i].append(mat.get(i, j))
	return arr_of_arr



def getTransformFromArr(arr_of_arr):
	mat = Matrix()
	for i, arr in enumerate(arr_of_arr):
		for j, val in enumerate(arr):
			mat.setElement(i, j, float(val))
	return mat



class Audio:
	Type = 0
	Tag = 'audio'

	@staticmethod
	def get_data(value):
		return {
			'event_name': value.readString('eventName', ''),
			'max_distance': value.readFloat('maxDistance', 0.0)
		}

	@staticmethod
	def set_data(value, new):
		value.writeString('eventName', new.event_name)
		value.writeFloat('maxDistance', new.max_distance)



class Model:
	Type = 1
	Tag = 'model'

	@staticmethod
	def get_data(value):
		return {
			'casts_shadow': value.readBool('castsShadow', True),
			'casts_local_shadow': value.readBool('castsLocalShadow', False),
			'reflection_visible': value.readBool('reflectionVisible', False),
			'resource': value.readString('resource'),
			'transform': getArrFromTransform(value)
		}

	@staticmethod
	def set_data(value, new):
		value.writeBool('castsShadow', new.casts_shadow)
		value.writeBool('castsLocalShadow', new.casts_local_shadow)
		value.writeBool('reflectionVisible', new.reflection_visible)
		value.writeString('resource', new.resource)
		value.writeMatrix('transform', getTransformFromArr(new.transform))



class OmniLight:
	Type = 2
	Tag = 'omniLight'

	@staticmethod
	def get_data(value):
		r, g, b = value.readVector3('colour').tuple()
		return {
			'cast_shadows': value.readString('castShadows'),
			'colour': {'r': r, 'g': g, 'b': b},
			'position': value.readVector3('position').tuple(),
			'inner_radius': value.readFloat('innerRadius', 0.0),
			'outer_radius': value.readFloat('outerRadius', 0.0),
			'multiplier': value.readFloat('multiplier', 0.0)
		}

	@staticmethod
	def set_data(value, new):
		value.writeBool('castShadows', new.cast_shadows)
		value.writeVector3('colour', (new.colour.r, new.colour.g, new.colour.b))
		value.writeVector3('position', tuple(float(s) for s in new.position))
		value.writeFloat('innerRadius', new.inner_radius)
		value.writeFloat('outerRadius', new.outer_radius)
		value.writeFloat('multiplier', new.multiplier)



class Particles:
	Type = 3
	Tag = 'particles'

	@staticmethod
	def get_data(value):
		return {
			'reflection_visible': value.readBool('reflectionVisible', False)
		}

	@staticmethod
	def set_data(value, new):
		value.writeBool('reflectionVisible', new.reflection_visible)



class PulseLight:
	Type = 4
	Tag = 'pulseLight'

	@staticmethod
	def get_data(value):
		return OmniLight.get_data(value)

	@staticmethod
	def set_data(value, new):
		OmniLight.set_data(value, new)



class PulseSpotLight:
	Type = 5
	Tag = 'pulseSpotLight'

	@staticmethod
	def get_data(value):
		return SpotLight.get_data(value)

	@staticmethod
	def set_data(value, new):
		SpotLight.set_data(value, new)



class Speedtree:
	Type = 6
	Tag = 'speedtree'

	@staticmethod
	def get_data(value):
		return {
			'casts_shadow': value.readBool('castsShadow', True),
			'casts_local_shadow': value.readBool('castsLocalShadow', False),
			'reflection_visible': value.readBool('reflectionVisible', False)
		}

	@staticmethod
	def set_data(value, new):
		value.writeBool('castsShadow', new.casts_shadow)
		value.writeBool('castsLocalShadow', new.casts_local_shadow)
		value.writeBool('reflectionVisible', new.reflection_visible)



class SpotLight:
	Type = 7
	Tag = 'spotLight'

	@staticmethod
	def get_data(value):
		r, g, b = value.readVector3('colour').tuple()
		return {
			'cast_shadows': value.readString('castShadows'),
			'colour': {'r': r, 'g': g, 'b': b},
			'position': value.readVector3('position').tuple(),
			'direction': value.readVector3('direction').tuple(),
			'inner_radius': value.readFloat('innerRadius', 0.0),
			'outer_radius': value.readFloat('outerRadius', 0.0),
			'multiplier': value.readFloat('multiplier', 0.0),
			'cone_angle': value.readFloat('coneAngle', 0.0)
		}

	@staticmethod
	def set_data(value, new):
		value.writeBool('castShadows', new.cast_shadows)
		value.writeVector3('colour', (new.colour.r, new.colour.g, new.colour.b))
		value.writeVector3('position', tuple(float(s) for s in new.position))
		value.writeVector3('direction', tuple(float(s) for s in new.direction))
		value.writeFloat('innerRadius', new.inner_radius)
		value.writeFloat('outerRadius', new.outer_radius)
		value.writeFloat('multiplier', new.multiplier)
		value.writeFloat('coneAngle', new.cone_angle)



TYPE_TO_CLASS = {
	Audio.Type: Audio,
	Model.Type: Model,
	OmniLight.Type: OmniLight,
	Particles.Type: Particles,
	PulseLight.Type: PulseLight,
	PulseSpotLight.Type: PulseSpotLight,
	Speedtree.Type: Speedtree,
	SpotLight.Type: SpotLight
}



TAG_TO_TYPE = {
	Audio.Tag: Audio.Type,
	Model.Tag: Model.Type,
	OmniLight.Tag: OmniLight.Type,
	Particles.Tag: Particles.Type,
	PulseLight.Tag: PulseLight.Type,
	PulseSpotLight.Tag: PulseSpotLight.Type,
	Speedtree.Tag: Speedtree.Type,
	SpotLight.Tag: SpotLight.Type
}



class SpaceEditorWindow(AbstractWindowView):

	data = None
	chunk = None

	@classmethod
	def clear(cls):
		cls.data = []
		cls.chunk = None

	@classmethod
	def addData(cls, id, tag, value):
		if tag not in TAG_TO_TYPE:
			return
		name = None
		if value.has_key('name'):
			name = value.readString('name')
		if value.has_key('resource'):
			name = value.readString('resource')
			name = os.path.basename(name)
		elif value.has_key('eventName'):
			name = value.readString('eventName')

		cls.data.append({
			'label': '%s (%s)' % (tag, name) if name else tag,
			'type': TAG_TO_TYPE[tag],
			'index': id
		})

	def __init__(self):
		AbstractWindowView.__init__(self)

	def _populate(self):
		AbstractWindowView._populate(self)
		self.initDP()

	def onWindowMinimize(self):
		self.__isMinimize = True
		self.flashObject.onWindowMinimize2()

	def onWindowClose(self):
		pass

	def py_log(self, message):
		print('SpaceEditorWindow:')
		print(message)

	def initDP(self):
		self.flashObject.setDataFromPy(self.data)

	def py_update(self, obj, viewData):
		val = self.chunk.values()[obj.index]
		TYPE_TO_CLASS[obj.type].set_data(val, viewData)
		self.chunk.save()
		g_hangarSpace.destroy()
		RefreshCls()
		g_hangarSpace.init(False)

	def py_get_data(self, obj):
		val = self.chunk.values()[obj.index]
		return TYPE_TO_CLASS[obj.type].get_data(val)


def init(ModHangarUtils):
	global RefreshCls
	RefreshCls = ModHangarUtils
	g_entitiesFactories.addSettings(ViewSettings('SpaceEditorWindow', SpaceEditorWindow, 'SpaceEditorWindow.swf', ViewTypes.WINDOW, None, ScopeTemplates.DEFAULT_SCOPE))

	def l_populate(self):
		#View._populate(self)
		self.flashObject.visible = False
		ModHangarUtils.hangarSpace.init(False)
		g_appLoader.getApp().loadView(SFViewLoadParams('SpaceEditorWindow', None))
	LoginView._populate = l_populate

	def l_dispose(self):
		View._dispose(self)
	LoginView._dispose = l_dispose

	def new_handleMouseEventGlobal(self, event):
		if self._ClientHangarSpace__isMouseDown:
			self.updateCameraByMouseMove(event.dx, event.dy, event.dz)
			return True
		return False

	ClientHangarSpace.handleMouseEventGlobal = new_handleMouseEventGlobal
